1)
Screeching Night Sound
BY ALEXANDER · OCTOBER 9, 2017

Description: Screeching night sound. Creepy eerie horror noises mp3 download. Cinematic tension sound. Best online SFX library for your projects.
Genres: Sound Effects
Artist: Alexander

The sound effect is permitted for non-commercial use under license “Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)”

http://www.orangefreesounds.com/screeching-night-sound

2)
Title: Enraged Zombies
About: Enraged pack of zombies charging a compound. they sound psychotic to say the least.
Uploaded: 05.26.11 
License: Attribution 3.0 
Recorded by Mike Koenig 

http://soundbible.com/1822-Enraged-Zombies.html

3)
Title: Bite
About: The sound of a monster bite or biting someone. great for those zombie games, or some cool sci fi monster scene. grrrr!
Uploaded: 09.03.09 
License: Attribution 3.0 
Recorded by Mike Koenig

http://soundbible.com/950-Bite.html

